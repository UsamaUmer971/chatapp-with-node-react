const chatForm = document.getElementById('chat-form');
const chatMessage = document.querySelector('.chat-messages');
const roomName = document.getElementById('room-name');
const userList = document.getElementById('users');

// Get username and room from URL

const {username, room} = Qs.parse(location.search,{
    ignoreQueryPrefix: true
});

const socket=io();

// Join chat room
socket.emit('joinRoom',{
    username,room
});

// Get room and users
socket.on('roomUsers',({room,users})=>{
    outputRoomName(room);
    outputUsers(users);
});

socket.on('message',message =>{
    outputMessage(message);
});

// Message Listen 
chatForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    // Get msg text
    const msg= e.target.elements.msg.value;
  
    // Emit msg to server
    socket.emit('chatMessage',msg);

    // Clear input 
    e.target.elements.msg.value ='';
    e.target.elements.msg.focus();

});

// Out msg to DOM
function outputMessage(message){
    const div= document.createElement('div');
    div.classList.add('message');
    div.innerHTML = `
    <p class="meta">${message.username} <span>${message.time}</span></p>
	<p class="text"> ${message.text} </p>`;
    chatMessage.appendChild(div);
}

// Add room to DOM
function outputRoomName(room){
    roomName.innerHTML = room;
};  
// Add user to DOM
function outputUsers(users){
    userList.innerHTML = `${users.map(user=>`<li>${user.username}</li>`).join('')}`;
}