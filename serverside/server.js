const path = require ('path');
const http = require ('http');
const express= require ('express');
const socketio = require('socket.io');
const formatMessage= require('./until/message');
const {userJoin, getCurrentUser, userLeave, getRoomUsers}= require('./until/users');


const app= express();
const server=http.createServer(app)
const io= socketio(server)

const PORT= 3000 || process.env.PORT;

const chatApp= "Chat APP"

// Set static folder    
app.use(express.static(path.join(__dirname,'../clientside')))
io.on('connection', socket =>{
    socket.on('joinRoom',({username, room})=>{

    const user = userJoin(socket.id,username,room) 
   
    socket.join(user.room);     

   // Welcom to user
   socket.emit('message', formatMessage(chatApp, 'Welcome to chat box'));

   // Brodcast when user connected
   socket.broadcast.to(user.room).emit('message', formatMessage(chatApp,`${user.username} has join the chat`));

    // Send user and room info
    io.to(user.room).emit('roomUsers',{
        room:user.room,
        users:getRoomUsers(user.room)
    });
});
  
    // Listen for chat msg
    socket.on('chatMessage', (msg)=>{
        const user = getCurrentUser(socket.id);

        io.to(user.room).emit('message', formatMessage(user.username,msg));

    });

    // When user disconnect
    socket.on('disconnect', ()=>{
        const user = userLeave(socket.id);
        if(user){
            io.to(user.room).emit('message', formatMessage(chatApp,`${user.username} left the chat`));

         // Send user and room info
        io.to(user.room).emit('roomUsers',{
        room:user.room,
        users:getRoomUsers(user.room)
    });
        }
        
    }); 
});

server.listen( PORT, ()=>{
    console.log(`Server is running on port ${PORT}.` );
});